<?php

require_once("connection.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $query = <<<PLSQL
insert into book (name, trip)
values (:name, :trip)
returning id into :id
PLSQL;

  $statement = oci_parse($c, $query);

  foreach(["name", "trip"] as $field) {
    oci_bind_by_name($statement, ":$field", $_POST[$field]);
  }

  oci_bind_by_name($statement, ":id", $id);

  if (oci_execute($statement)) {
    $query = <<<PLSQL
select
  book.code,
  book.name,
  book.trip,
  rise.name as risename,
  dest.name as destname,
  to_char(trip.when, 'YYYY-MM-DD"T"HH24:MI:SS') as when,
  trip.cost as cost,
  book.id
from book
join trip on book.trip = trip.id
join city rise on trip.rise = rise.code
join city dest on trip.dest = dest.code
where book.id = :id
PLSQL;

    $statement = oci_parse($c, $query);
    oci_bind_by_name($statement, ":id", $id);
    oci_execute($statement);
    oci_fetch_all($statement, $data, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);

    $xml = new SimpleXMLElement("<root/>");
    array_to_xml($data, $xml, "trip");

    header("Content-type: text/xml;charset=utf-8");
    echo $xml->asXML();
  }
}
elseif($_SERVER["REQUEST_METHOD"] == "GET") {
  $query = <<<PLSQL
select
  book.code,
  book.name,
  book.trip,
  rise.name as risename,
  dest.name as destname,
  to_char(trip.when, 'YYYY-MM-DD"T"HH24:MI:SS') as when,
  trip.cost as cost,
  book.id
from book
join trip on book.trip = trip.id
join city rise on trip.rise = rise.code
join city dest on trip.dest = dest.code
where
  (:code is null or book.code = :code) and
  (:name is null or book.name = :name) and
  (:trip is null or book.trip = :trip) and
  (:id is null or book.id = :id)
PLSQL;

  $statement = oci_parse($c, $query);

  foreach(["code", "name", "trip", "id"] as $field) {
    oci_bind_by_name($statement, ":$field", $_GET[$field]);
  }

  oci_execute($statement);
  oci_fetch_all($statement, $data, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);

  $xml = new SimpleXMLElement("<root/>");
  array_to_xml($data, $xml, "book");

  header("Content-type: text/xml;charset=utf-8");
  echo $xml->asXML();
}
