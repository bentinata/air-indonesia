# Air Indonesia

---

# Requirement
* Oracle XE 11g
[Docker build](https://github.com/wnameless/docker-oracle-xe-11g) available.
* PHP > 5.6
* PHP OCI8 (OCI binding optional)
* PHP XML

---

# Preparation

* change `config.php`
* import SQL;
all data are generated

```
$ sqlplus /nolog username/password@//hostname @air-indonesia.sql
```

---

# Usage
```
$ php -S 127.0.0.1:8000
```

Or just puts things into `htdocs`.

---

# Docker
```
docker run -d -p 1521:1521 wnameless/oracle-xe-11g
```

```
hostname: localhost
port: 49161
sid: xe
username: system
password: oracle
linux user: root
linux pass: admin
```

---
