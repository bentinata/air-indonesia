<!DOCTYPE html>
<html>
  <head>
    <?php require("head.php"); ?>
  </head>
  <body>
    <section class="hero is-fullheight">
      <div class="hero-body">
        <div class="container">

          <p class="control has-icon">
          <input class="input is-large" placeholder="Find flights from..." list="rise"></input>
          <i class="fa fa-arrow-up"></i>
          </p>
          <datalist id="rise"></datalist>

          <p class="control has-icon">
          <input class="input is-large" placeholder="Find flights to..." list="dest"></input>
          <i class="fa fa-arrow-down"></i>
          </p>
          <datalist id="dest"></datalist>

          <div id="available-trip" class="columns is-multiline is-mobile">
            <div class="already column is-one-quarter-desktop is-half-mobile">
              <div class="card is-fullwidth">
                <div class="card-content">
                  <div class="content">
                    <p>Already have a ticket?</p>
                    <p>Click here.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>
    <div id="book" class="modal">
      <div class="modal-background"></div>
      <div class="modal-content">
        <div class="card is-fullwidth">
          <header class="card-header">
            <p class="card-header-title">
            Trip Detail
            </p>
          </header>
          <div class="card-content">
            <div class="content">
              <form>
                <input id="trip-id" style="display:none"></input>
                From:
                <p id="trip-rise" class="title"></p>
                To:
                <p id="trip-dest" class="title"></p>
                Date & time:
                <p id="trip-when" class="title"></p>
                Price:
                <p id="trip-cost" class="title"></p>


                <p class="control">
                <input id="book-name" class="input is-large" placeholder="Full Name"></input>
                <p>
                <button id="book-submit" type="submit" class="button is-large is-fullwidth is-disabled">Book</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="already" class="modal">
      <div class="modal-background"></div>
      <div class="modal-content">
        <div class="card is-fullwidth">
          <header class="card-header">
            <p class="card-header-title">
            Find Booking
            </p>
          </header>
          <div class="card-content">
            <div class="content">
              <form>
                <p class="control">
                <input id="already-code" class="input is-large" placeholder="Booking Code"></input>
                <p>
                <button id="already-submit" type="submit" class="button is-large is-fullwidth is-disabled">Find</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="detail" class="modal">
      <div class="modal-background"></div>
      <div class="modal-content">
        <div class="card is-fullwidth">
          <header class="card-header">
            <p class="card-header-title">
            Booking Detail
            </p>
          </header>
          <div class="card-content">
            <div class="content">
              Passenger:
              <p id="detail-name" class="title"></p>
              Code:
              <p id="detail-code" class="title"></p>
              From:
              <p id="detail-rise" class="title"></p>
              To:
              <p id="detail-dest" class="title"></p>
              Date & time:
              <p id="detail-when" class="title"></p>
              Price:
              <p id="detail-cost" class="title"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require("script.php"); ?>
    <script type="text/javascript">
function parseXML(response) {
  return response.text()
    .then(text => (new DOMParser()).parseFromString(text, `text/xml`));
}

function thousandSeparator(n) {
  return `${n}`.replace(/\B(?=(\d{3})+(?!\d))/g, `.`);
}

function fillCity(xml) {
  const $xml = $(xml);

  $xml.find(`city`).each((i, e) => {
    const $e = $(e);
    const $option = $(`<option>`)
      .val($e.find(`code`).text())
      .html($e.find(`name`).text());

    $(`#rise,#dest`).append($option);
  });
}

function fillTrip(xml) {
  const $xml = $(xml);
  const $availableTrip = $(`#available-trip`);
  $availableTrip.empty();

  $xml.find(`trip`).each((i, e) => {
    let trip = `<?php require("trip-card.php"); ?>`;

    $(e).children().each((i, e) => {
      const tag = $(e).prop(`tagName`);
      let text = $(e).text();

      if (tag === `when`)
        text = moment(text).format(`ddd, D MMM H:mm`);

      if (tag === `cost`)
        text = thousandSeparator(text);

      trip = trip.replace(`{{ ${tag} }}`, text);
    });

    $availableTrip.append($(trip));
  });
}

function checkTrip() {
  const query = {
    rise: $(`input[list=rise]`).val(),
    dest: $(`input[list=dest]`).val(),
  };

  if (query.rise || query.dest) {
    fetch(`trip.php?${$.param(query)}`, { method: `GET` })
      .then(parseXML)
      .then(fillTrip)
  }
}

function fillBook(xml) {
  closeModal();

  const $bookModal = $(`#book`);
  const $xml = $(xml);
  $bookModal
    .find(`#trip-id`)
    .val($xml.find(`id`).text());
  $bookModal
    .find(`#trip-rise`)
    .text($xml.find(`risename`).text());
  $bookModal
    .find(`#trip-dest`)
    .text($xml.find(`destname`).text());
  $bookModal
    .find(`#trip-cost`)
    .text(`Rp${thousandSeparator($xml.find(`cost`).text())}`);
  $bookModal
    .find(`#trip-when`)
    .text(moment($xml.find(`when`).text()).format(`dddd, D MMMM H:mm`));

  $bookModal.addClass(`is-active`);
}

function fillDetail(xml) {
  closeModal();

  const $detailModal = $(`#detail`);
  const $xml = $(xml);
  $detailModal
    .find(`#detail-name`)
    .text($xml.find(`name`).text());
  $detailModal
    .find(`#detail-code`)
    .text($xml.find(`code`).text());
  $detailModal
    .find(`#detail-rise`)
    .text($xml.find(`risename`).text());
  $detailModal
    .find(`#detail-dest`)
    .text($xml.find(`destname`).text());
  $detailModal
    .find(`#detail-when`)
    .text(moment($xml.find(`when`).text()).format(`dddd, D MMMM H:mm`));
  $detailModal
    .find(`#detail-cost`)
    .text(`Rp${thousandSeparator($xml.find(`cost`).text())}`);

  $detailModal.addClass(`is-active`);
}

function closeModal() {
  $(`.modal`).find(`form`).each((i, e) => e.reset());
  $(`.modal`).removeClass(`is-active`);
}

fetch(`city.php`, { method: `GET` })
  .then(parseXML)
  .then(fillCity);

$(`input[list=rise],input[list=dest]`).on(`change`, checkTrip);

checkTrip();

$(document).on(`click`, `.trip`, (e) => {
  const $bookModal = $(`#book`);
  const id = $(e.target).closest(`.trip`).find(`.trip-id`).text();

  fetch(`trip.php?id=${id}`, { method: `GET` })
    .then(parseXML)
    .then(fillBook)
});

$(`.already`).on(`click`, (e) => {
  $(`#already`).addClass(`is-active`);
});

$(`.modal-background, .modal-close`).on(`click`, closeModal);

setInterval(() => {
  if ($(`#book-name`).val())
    $(`#book-submit`).removeClass(`is-disabled`);
  else
    $(`#book-submit`).addClass(`is-disabled`);

  if ($(`#already-code`).val())
    $(`#already-submit`).removeClass(`is-disabled`);
  else
    $(`#already-submit`).addClass(`is-disabled`);
}, 200);


$(`#book-submit`).on(`click`, (e) => {
  e.preventDefault();
  const $bookModal = $(`#book`);

  const data = new FormData();
  data.append(`trip`, $bookModal.find(`#trip-id`).val());
  data.append(`name`, $bookModal.find(`#book-name`).val());

  fetch(`book.php`, {
    method: `POST`,
    header: {
      'Content-Type': `multipart/form-data`,
    },
    body: data,
  })
    .then(parseXML)
    .then(fillDetail);
});

$(`#already-submit`).on(`click`, (e) => {
  e.preventDefault();
  const $alreadyModal = $(`#already`);
  const code = $(`#already-code`).val();

  fetch(`book.php?code=${code}`, {
    method: `GET`,
  })
    .then(parseXML)
    .then(fillDetail);

});
    </script>
  </body>
</html>
