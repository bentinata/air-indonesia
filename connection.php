<?php

require_once "config.php";
$c = oci_pconnect($config["db_user"], $config["db_pass"], $config["db_cstr"]);

function array_to_xml($data, &$xml, $name) {
  foreach($data as $key => $value) {
    if (is_numeric($key)) {
      $key = $name;
    }
    if (is_array($value)) {
      $subnode = $xml->addChild($key);
      array_to_xml($value, $subnode, $name);
    }
    else {
      $xml->addChild(strtolower($key), htmlspecialchars("$value"));
    }
  }
}
