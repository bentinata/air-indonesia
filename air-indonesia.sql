drop table book;
drop sequence sq_book;
drop trigger tr_book_insert;

drop table trip;
drop sequence sq_trip;
drop trigger tr_trip_insert;

drop table city;
drop sequence sq_city;
drop trigger tr_city_insert;

create or replace function to_unix(ts in date) return number is
  unix number;
begin
  unix := (ts - date '1970-01-01') * 60 * 60 * 24;
  return unix;
end;
/

create or replace function fn_code(n number) return varchar2 is
  code varchar2(64);
  c varchar2(64);
  i number;
begin
  c := '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  code := '';
  i := n;
  while i > 0 loop
    code := concat(substr(c, mod(i, length(c)) + 1, 1), code);
    i := floor(i/length(c));
  end loop;
  return code;
end;
/

create table city (
  code varchar2(3),
  name varchar2(64),
  lng number,
  lat number,
  constraint uniq_code
    unique(code),
  id number primary key
);

create sequence sq_city;
create trigger tr_city_insert
  before insert
  on city
  for each row
begin
  :new.id := sq_city.nextval;
end;
/

insert all
  into city(code, name, lng, lat) values ('CGK', 'Jakarta - Soekarno Hatta', -6.12, 106.49)
  into city(code, name, lng, lat) values ('SUB', 'Surabaya - Juanda International', -6.12, 106.49)
  into city(code, name, lng, lat) values ('DPS', 'Denpasar - Ngurah Rai', -6.12, 106.49)
  into city(code, name, lng, lat) values ('UPG', 'Makassar - Sultan Hassanuddin', -6.12, 106.49)
  into city(code, name, lng, lat) values ('KNO', 'Medan - Kuala Namo', -6.12, 106.49)
  into city(code, name, lng, lat) values ('BPN', 'Balikpapan - Sultan Aji Muhammad Sulaiman', -6.12, 106.49)
  into city(code, name, lng, lat) values ('JOG', 'Yogyakarta - Adisucipto', -6.12, 106.49)
  into city(code, name, lng, lat) values ('BTH', 'Batam - Hang Nadim', -6.12, 106.49)
  into city(code, name, lng, lat) values ('SRG', 'Semarang - Achmad Yani', -6.12, 106.49)
  into city(code, name, lng, lat) values ('BDJ', 'Banjarmasin - Syamsudin Noor', -6.12, 106.49)
  into city(code, name, lng, lat) values ('PLM', 'Palembang - Sultan Mahmud Badaruddin II', -6.12, 106.49)
  into city(code, name, lng, lat) values ('PDG', 'Padang - Minangkabau', -6.12, 106.49)
  into city(code, name, lng, lat) values ('BDO', 'Bandung - Husein Sastranegara', -6.12, 106.49)
  into city(code, name, lng, lat) values ('HLP', 'Jakarta - Halim Perdanakusma', -6.12, 106.49)
  into city(code, name, lng, lat) values ('PNK', 'Pontianak - Supadio', -6.12, 106.49)
  into city(code, name, lng, lat) values ('PKU', 'Pekanbaru - Sultan Syarif Kasim II International', -6.12, 106.49)
  into city(code, name, lng, lat) values ('SMQ', 'Sampit - Sampit', -6.12, 106.49)
  into city(code, name, lng, lat) values ('LOP', 'Mataram - Lombok International', -6.12, 106.49)
  into city(code, name, lng, lat) values ('MDC', 'Manado - Sam Ratulangi', -6.12, 106.49)
  into city(code, name, lng, lat) values ('DJJ', 'Jayapura - Sentani', -6.12, 106.49)
  into city(code, name, lng, lat) values ('PGK', 'Pangkal Pinang - Depati Amir', -6.12, 106.49)
  into city(code, name, lng, lat) values ('SOC', 'Solo - Adisumarmo International', -6.12, 106.49)
  into city(code, name, lng, lat) values ('KOE', 'Kupang - El Tari', -6.12, 106.49)
  into city(code, name, lng, lat) values ('TKG', 'Bandar Lampung - Radin Inten II', -6.12, 106.49)
  into city(code, name, lng, lat) values ('AMQ', 'Ambon - Pattimura', -6.12, 106.49)
  into city(code, name, lng, lat) values ('DJB', 'Jambi City - Sultan Thaha', -6.12, 106.49)
  into city(code, name, lng, lat) values ('TRK', 'Tarakan - Juwata International', -6.12, 106.49)
  into city(code, name, lng, lat) values ('BTJ', 'Banda Aceh - Sultan Iskandar Muda International', -6.12, 106.49)
  into city(code, name, lng, lat) values ('PLW', 'Palu - Mutiara', -6.12, 106.49)
  into city(code, name, lng, lat) values ('BKS', 'Bengkulu City - Fatmawati Soekarno', -6.12, 106.49)
  into city(code, name, lng, lat) values ('PKY', 'Palangkaraya - Tjilik Riwut', -6.12, 106.49)
  into city(code, name, lng, lat) values ('MKW', 'Manokwari - Rendani', -6.12, 106.49)
  into city(code, name, lng, lat) values ('TIM', 'Timika - Timika', -6.12, 106.49)
  into city(code, name, lng, lat) values ('TJQ', 'Tanjung Pandan - H.A.S. Hanandjoeddin', -6.12, 106.49)
  into city(code, name, lng, lat) values ('GTO', 'Gorontalo City - Jalaluddin', -6.12, 106.49)
  into city(code, name, lng, lat) values ('SOQ', 'Sorong - Sorong', -6.12, 106.49)
  into city(code, name, lng, lat) values ('TTE', 'Ternate - Sultan Babullah', -6.12, 106.49)
  into city(code, name, lng, lat) values ('BIK', 'Biak - Frans Kaisiepo', -6.12, 106.49)
  into city(code, name, lng, lat) values ('MKQ', 'Merauke - Mopah', -6.12, 106.49)
  into city(code, name, lng, lat) values ('PKN', 'Pangkalan Bun - Iskandar', -6.12, 106.49)
  into city(code, name, lng, lat) values ('MLG', 'Malang - Abdul Rachman Saleh', -6.12, 106.49)
  into city(code, name, lng, lat) values ('TNJ', 'Tanjung Pinang - Raja Haji Fisabilillah', -6.12, 106.49)
  into city(code, name, lng, lat) values ('WMX', 'Wamena - Wamena', -6.12, 106.49)
  into city(code, name, lng, lat) values ('GNS', 'Gunung Sitoli - Binaka', -6.12, 106.49)
  into city(code, name, lng, lat) values ('ENE', 'Ende - H. Hasan Aroeboesman', -6.12, 106.49)
  into city(code, name, lng, lat) values ('KTG', 'Ketapang - Ketapang', -6.12, 106.49)
  into city(code, name, lng, lat) values ('LBJ', 'Labuan Bajo - Labuhan Baju', -6.12, 106.49)
  into city(code, name, lng, lat) values ('SRI', 'Samarinda - Temindung', -6.12, 106.49)
  into city(code, name, lng, lat) values ('BUW', 'Bau-Bau - Betoambari', -6.12, 106.49)
  into city(code, name, lng, lat) values ('BMU', 'Bima - Sultan Muhammad Salahuddin', -6.12, 106.49)
select * from dual;

create table trip (
  rise varchar2(3),
  dest varchar2(3),
  cost number,
  when timestamp,
  seat number,
  constraint fk_rise
    foreign key (rise)
    references city(code)
    on delete cascade,
  constraint fk_dest
    foreign key (dest)
    references city(code)
    on delete cascade,
  id number primary key
);

create sequence sq_trip;
create trigger tr_trip_insert
  before insert
  on trip
  for each row
begin
  :new.id := sq_trip.nextval;
end;
/

create table book (
  code varchar2(16),
  name varchar2(32),
  trip number,
  constraint fk_trip
    foreign key (trip)
    references trip(id)
    on delete cascade,
  id number primary key
);

create sequence sq_book;
create trigger tr_book_insert
  before insert
  on book
  for each row
declare
  seat number;
begin
  :new.id := sq_book.nextval;

  select seat into seat from trip
    where id = :new.trip;

  :new.code := fn_code(to_unix(systimestamp));

  update trip set seat = seat - 1
    where id = :new.trip;
end;
/


create or replace procedure mk_trip (
  n in number,
  cost_min in number,
  cost_max in number,
  seat_min in number,
  seat_max in number
)
as
  i number;
  rise varchar2(3);
  dest varchar2(3);
  cost number;
  bout timestamp;
  seat number;
begin
  i := 1;
  for i in 1..n loop
    select code into rise from (
      select code from city order by dbms_random.value
    )
    where rownum <= 1;

    select code into dest from (
      select code from city order by dbms_random.value
    )
    where rownum <= 1;

    select floor(dbms_random.value(cost_min, cost_max)) into cost from dual;
    select floor(dbms_random.value(seat_min, seat_max)) into seat from dual;
    select sysdate + dbms_random.value(0, sysdate - sysdate + 90) into bout from dual;

    insert into trip (rise, dest, cost, when, seat)
      values (rise, dest, cost, bout, seat);
  end loop;
end;
/

call mk_trip(100, 400000, 3000000, 120, 200);

exit;
