<!DOCTYPE html>
<html>
  <head>
    <title>Title</title>
    <meta charset="utf-8">
    <style>
      body {
        font-family: serif;
      }

      h1, h2, h3 {
        font-family: sans-serif;
        font-weight: normal;
      }

      .remark-code, .remark-inline-code {
        font-family: monospace;
      }
    </style>
  </head>
  <body>
    <textarea id="source">
class: center, middle

<?php require("README.md"); ?>
<?php require("SLIDE.md"); ?>
    </textarea>
    <script src="https://gnab.github.io/remark/downloads/remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create();
    </script>
  </body>
</html>
