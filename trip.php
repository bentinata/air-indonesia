<?php

require_once("connection.php");

$query = <<<PLSQL
select
  rise,
  rise.name as risename,
  dest,
  dest.name as destname,
  cost,
  to_char(when, 'YYYY-MM-DD"T"HH24:MI:SS') as when,
  seat,
  trip.id
from trip
join city rise on trip.rise = rise.code
join city dest on trip.dest = dest.code
where
  (:rise is null or rise = :rise) and
  (:dest is null or dest = :dest) and
  (:id is null or trip.id = :id)
PLSQL;

$statement = oci_parse($c, $query);

foreach(["rise", "dest", "id"] as $field) {
  oci_bind_by_name($statement, ":$field", $_GET[$field]);
}

oci_execute($statement);
oci_fetch_all($statement, $data, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);

$xml = new SimpleXMLElement("<root/>");
array_to_xml($data, $xml, "trip");

header("Content-type: text/xml;charset=utf-8");
echo $xml->asXML();
