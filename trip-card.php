<div class="trip column is-one-quarter-desktop is-half-mobile">
  <div class="card is-fullwidth">
    <div class="card-content">
      <div class="content">
        <div class="trip-id" style="display:none">{{ id }}</div>
        <p>
          <span class="tag">{{ rise }}</span>
          <span class="tag">{{ dest }}</span>
        </p>
        <p>{{ when }}</p>
        <p>Rp{{ cost }}</p>
      </div>
    </div>
  </div>
</div>
