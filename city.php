<?php

require_once("connection.php");

$query = oci_parse($c, "select code, name, id from city");
oci_execute($query);
oci_fetch_all($query, $data, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);

$xml = new SimpleXMLElement('<root/>');
array_to_xml($data, $xml, "city");

header("Content-type: text/xml;charset=utf-8");
echo $xml->asXML();
